const {format, transports} = require("winston");

module.exports = {
    // level: 'info',
    silent: process.env.NODE_ENV === "test", // only for unit/integration testing
    defaultMeta: {ok : true},
    format: format.combine.apply(format, [
        format.timestamp(),
        process.env.NODE_ENV === "development" ? format.colorize(): undefined,
        format.splat(),
        format.printf(({meta, level, timestamp, message}) =>
            [
                timestamp,
                '-',
                level,
                '@',
                String(message).trim() + (meta ? ':' : ''),
                typeof meta === "object" ? JSON.stringify(meta) : meta,
                "\n"
            ].join(' ')
        )
    ].filter( i => i)),
    transports: [new transports.Console()]
};
