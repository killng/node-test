module.exports = {
    // eslint-disable-next-line max-len
    morganFormat: ':response-time ms - :remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"'
};