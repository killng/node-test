const SELLER = 'SellerCompany';
const GET = 'get';
const CountryNames = {
    CL: 'chile',
    CO: 'colombia',
    AR: 'argentina',
    PE: 'peru'
}

const FALABELLA = 'falabella';
const SODIMAC = 'sodimac';
const BUSINESS_ID = { [FALABELLA]: 0, [SODIMAC]: 1 };

// Courier FBS Retira: Chile, Peru, Argentina
const CROSS_DOCKING_WITH_LOGISTIC_TRAIN = 'cross_docking_with_logistic_train';

// Courier FBS Entrega: Chile, Peru, Argentina
const CROSS_DOCKING_EXPRESS = 'cross_docking_express';

// Courier FBS Entrega en chilexpress: Chile
const CROSS_DOCKING_WITH_3PL = 'cross_docking_with_3pl';

// Courier FBS Retira: Colombia
const EXTERNAL_COURIER_EXPRESS_DEPRISA = 'external_courier_express_deprisa';

// Courier Internacional
const EXTERNAL_COURIER_EXPRESS = 'external_courier_express';

// Courier Internacional
const EXTERNAL_COURIER_EXPRESS_ASIA = 'external_courier_express_asia';

// Maximum days that the filter allows
const MAX_DAYS_ON_FILTER_ADMIN = 90;

module.exports = {
  SELLER,
  GET,
  CountryNames,
  CROSS_DOCKING_WITH_LOGISTIC_TRAIN,
  CROSS_DOCKING_EXPRESS,
  CROSS_DOCKING_WITH_3PL,
  EXTERNAL_COURIER_EXPRESS_DEPRISA,
  EXTERNAL_COURIER_EXPRESS,
  EXTERNAL_COURIER_EXPRESS_ASIA,
  MAX_DAYS_ON_FILTER_ADMIN,
  BUSINESS_ID
}