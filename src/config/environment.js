require('dotenv').config();

const {
  USERS_URL
} = process.env;

module.exports = {
  USERS_URL
};
