const logger = require("../helpers/logger")
let cacheEnabled = true;
if (process.env.REDIS_CONNECTION_HOST) {
    logger.log("info", "redis ok")
    const mongoose = require('src/config/mongoose');
    const cachegoose = require('cachegoose');
    cachegoose(mongoose, {
        engine: 'redis',
        port: process.env.REDIS_CONNECTION_PORT || 6379,
        host: process.env.REDIS_CONNECTION_HOST
    })
} else {
    cacheEnabled = false;
    logger.info("Redis cache not configured")
}

module.exports = cacheEnabled