const {
    MONGO_SCHEMA, MONGO_USERNAME, MONGO_PASSWORD, MONGO_HOSTNAME, MONGO_DATABASE, MONGO_PORT, MONGO_AUTH_SOURCE,
    MONGO_AUTH_MECHANISM, MONGO_SSL, MONGO_REPLICA_SET, MONGO_POOL_SIZE
} = process.env;

const query = [];
const connection = [MONGO_SCHEMA  || 'mongodb', '://'];
if (MONGO_USERNAME) {
    connection.push(encodeURIComponent(MONGO_USERNAME));
    if (MONGO_PASSWORD) connection.push(':', encodeURIComponent(MONGO_PASSWORD));
    connection.push('@');
}
connection.push(MONGO_HOSTNAME);
if (MONGO_PORT) {
    connection.push(':', MONGO_PORT);
}
if (MONGO_DATABASE) {
    connection.push('/', MONGO_DATABASE);
}
if (MONGO_AUTH_SOURCE) {
    query.push('authSource=' + MONGO_AUTH_SOURCE);
}
if (MONGO_AUTH_MECHANISM) {
    query.push('authMechanism=' + MONGO_AUTH_MECHANISM);
}
if (MONGO_SSL) {
    query.push('ssl=' + MONGO_SSL);
}
if (query.length > 0) {
    connection.push('?', query.join('&'))
}
module.exports = {
    connectionString: connection.join(''),
    options: {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        autoIndex: false, // Don't build indexes
        reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
        reconnectInterval: 500, // Reconnect every 500ms
        poolSize: parseInt(MONGO_POOL_SIZE) || 5,
        bufferMaxEntries: 0, // If not connected, return errors immediately rather than waiting for reconnect
        connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
        socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
        family: 4, // Use IPv4, skip trying IPv6
        useUnifiedTopology: false
    }
};