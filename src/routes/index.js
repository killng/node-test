const express = require('express');
const orders =  require("../controllers/orders/index");
const gustavs =  require("../controllers/gustavs/index");
//const massProcess = require("../controllers/mass-process");
//const externalCourierOrders = require('../controllers/external-courier-order');
//const rejectedDistributionOrder = require("../controllers/rejected-distribution-order");

const router = express.Router();

router.get('/', orders);
router.get('/gustavs', gustavs);
//router.get('/mass_processes/:id', massProcess.findOne);
//router.get('/external-courier-orders/count', externalCourierOrders.count)
//router.get('/external-courier-orders', externalCourierOrders.findMany)
//router.get('/rejected_distribution_order', rejectedDistributionOrder);

module.exports = router

