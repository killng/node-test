const request = require('request');
const logger = require("./logger");

/**
 * @param {Options} options
 * @return {Promise}
 * */
exports.request = options => {
  logger.info('Staring request to: ', options);
  return new Promise((resolve, reject) => {
    request[options.method](options, (error, response) => {
      if (error) {
        logger.error(error.message, error);
        return reject({statusCode: 500, message: error.message});
      } else {
        logger.info('Request response: ', {
          url: options.url,
          status: response.statusCode
        })
        if (response.statusCode < 200 || response.statusCode > 299) {
          return reject({statusCode: response.statusCode, message: response.body});
        }
        return resolve(response);
      }
    });
  });
};