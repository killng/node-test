const {createLogger} = require("winston");
module.exports = createLogger(require('../config/logger'));