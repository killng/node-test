const countries = {
    'chile': 'CL',
    'peru': 'PE',
    'colombia': 'CO',
    'argentina': 'AR',
    'cl': 'CL',
    'pe': 'PE',
    'co': 'CO',
    'ar': 'AR'
};
/**
 * @param {String} name
 * @return {String}
 * */
const mapCountry = name => countries[String(name || "cl").toLowerCase()];

module.exports = {countries, mapCountry}