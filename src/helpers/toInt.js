/**
 * This function convert a value to (safe) integer
 * You don't need lodash to make this!
 *
 * @param {*} value
 * @param {Number} [defaultValue]
 * @param {Boolean} [safe]
 * @return {Number}
 * */
module.exports = (value, defaultValue = 0, safe= true) => {
    value = Number(value);
    if (isNaN(value)) return typeof defaultValue === "number" ? defaultValue : 0;
    else if (safe) return Math.min(value, Number.MAX_SAFE_INTEGER);
    return value
};