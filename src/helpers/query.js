const moment = require("moment");
/**
 * @typedef Between
 * @property {Date} $gte
 * @property {Date} [$lt]
 * */
/**
 * @param {String|Number|Boolean|Object} value
 * @return {String|Number|Boolean}
 * */
const queryCreateEq = (value) => {
    if (!value && typeof value !== "number") return undefined;
    if (["number", "string", "boolean"].indexOf(typeof value) === -1) return JSON.stringify(value);
    return value;
};

/**
 * @param {Date|String} from
 * @param {Date|String} [to]
 * @param {Boolean} [convert]
 * @return {Between | undefined}
 * */
const queryCreateBetweenDate = (from, to = undefined, convert = false) => {
    if (convert === true) {
        from = typeof from === "string" ? moment(from, 'YYYY-MM-DD').hour(0).minute(0).second(0).toDate() : from;
        to = typeof to === "string" ? moment(to, 'YYYY-MM-DD').hour(23).minute(59).second(59).millisecond(999).toDate() : to;
    }
    const cond = {$gte: from}
    if (to) cond.$lt = to;
    return cond;
};
/**
 * @param {*[]|String} value
 * @return {String[] | String}
 * */
const queryCreateIn = (value) => {
    if (Array.isArray(value) && value.length > 0) {
        const tmp = value.filter(i => ["number", "boolean", "string"].indexOf(typeof i) > -1);
        if (tmp.length === 0) return  undefined;
        else if (tmp.length === 1) return tmp[0];
        else return  {$in: tmp};
    } else if (typeof value == "string") return value;
    return undefined;
};

/**
 * @param {Array} _array
 * @param {String[]} allow
 * @return {Object}
 * */
const interpretCondArray = (_array, allow) => {
    const data = {};
    const basicTypes = ["number", "string", "boolean"];
    if (Array.isArray(_array)) {
        for (let item of _array) {
            if (!item || Array.isArray(item) || typeof item !== "object") continue;
            for (let property in item) {
                if (allow.indexOf(property) === -1 || basicTypes.indexOf(typeof item[property]) === -1) {
                    continue;
                }
                const value = item[property];
                if (!Array.isArray(data[property])) {
                    data[property] = typeof data[property] !== "undefined" ? [data[property]] : [];
                }
                data[property].push(value);
            }
        }
    }
    return data;
};
/**
 *
 * */
const isObject = value => typeof value === "object";
/**
 *
 * */
const isPlainObject = (value) => {
    if (Object.prototype.toString.call(value) !== '[object Object]') return false;
    const prototype = Object.getPrototypeOf(value);
    return prototype === null || prototype === Object.prototype;
};
/**
 * @param {Array<Object>} notSafe
 * @return {undefined | Array<Object>}
 * */
const safeOrOperator = (notSafe) => {
    let safe = notSafe;
    if (Array.isArray(safe) && safe.length > 0) {
        for (let i = 0; i < safe.length; i++) {
            if (!isPlainObject(safe[i])) {
                safe.splice(i, 1);
                continue;
            }
            for (let property in safe[i]) {
                if (safe[i][property] instanceof Date) safe[i][property] = safe[i][property].toISOString()
                else if (["number", "string", "boolean"].indexOf(typeof safe[i][property]) === -1) delete safe[i][property];
            }
        }
        if (safe.length === 0) {
            return undefined;
        }
    } else {
        return undefined;
    }
    return safe;
};
/**
 * Group items from an array together by some criteria or value.
 * @param  {Array}           arr      The array to group items from
 * @param  {String|Function} criteria The criteria to group by
 * @return {Object}                   The grouped object
 */
const groupBy = (arr, criteria) =>
    arr.reduce((obj, item) => {
        const key = typeof criteria === 'function' ? criteria(item) : item[criteria];
        if (!obj.hasOwnProperty(key)) obj[key] = [];
        obj[key].push(item);
        return obj;
    }, {});

/**
 * SVL for now only sorts by one field at a time so it is treated as such.
 * @param {String} [sort] - Sort value in the format 'createdAt desc'
 * @example
 * sortBy('createdAt desc');
 * // returns
 * {
 *     createdAt: 'desc'
 * }
 * @returns {Object} Object with sort values
 */
const sortBy = (sort) => {
    if (!sort || typeof sort !== 'string') return;
    const DIRECTIONS = ['asc', 'desc'];
    let [property, direction] = sort.split(' ');
    direction = `${direction}`.toLowerCase();
    if (!DIRECTIONS.includes(direction)) return;
    return {
        [property]: direction
    };
};

/**
 * Formats a projection object and deletes invalid values
 * @param {Object} projection - key value pairs of property and 0 or 1 as value
 * @returns {Object} Formatted projection
 */
const formatProjection = projection => {
    if (!projection || typeof projection !== 'object') return;
    Object.keys(projection).forEach(key => {
        const value = projection[key];
        const numericValue = Number(value);
        if (value === null || isNaN(numericValue) || (numericValue !== 0 && numericValue !== 1)) {
            // Projection only accepts 0s and 1s
            delete projection[key];
        } else {
            projection[key] = numericValue;
        }
    })
}

module.exports = {
  safeOrOperator,
  isObject,
  isPlainObject,
  groupBy,
  queryCreateEq,
  queryCreateBetweenDate,
  queryCreateIn,
  interpretCondArray,
  sortBy,
  formatProjection
};
