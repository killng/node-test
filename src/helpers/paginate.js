const logger = require("./logger");
const toInt = require("./toInt");
const clearObject = require('./clearObject');
/**
 * @typedef Pagination
 * @property {Array} page
 * @property {Number} count
 * @property {Number} total_pages
 * @property {Number} total_count
 * @property {Number} current_page
 * @property {Number} next_page
 * */
/**
 * @param {Object} collection
 * @param {Object} [conditions]
 * @param {Number} [limit]
 * @param {Number} [page]
 * @param {Object} [sort]
 * @param {Boolean} [lean]
 * @param {Number} [cache]
 * @param {Object} [projection]
 * @return {Promise<Pagination>}
 * */
module.exports = async (
    collection,
    conditions = {},
    limit = 10,
    page = 0,
    sort= {},
    lean = true,
    cache = 0,
    projection = null
) => {
    limit === 0 ? null : limit = Math.abs(toInt(limit) || 50);
    page = Math.abs(toInt(page) || 0);
    cache = Math.abs(toInt(cache) || 0);
    const skip = Math.max(0, (page - 1) * limit) ||undefined;

    // remove undefined properties
    clearObject(conditions);
    
    let find = collection.find(conditions, projection, {limit, skip, sort});
    if (lean) find = find.lean();
    if (typeof find.cache === "function" && cache) find = find.cache(cache);

    let count = 0,
        docs = [],
        startTime = new Date();
    try {
        count = await collection.count(conditions).exec()
        docs = await find.exec()
    } catch (e) {
        logger.error(e.message, e)
    }
    logger.info(`Pagination take ${ new Date() - startTime}ms`)
    const total_pages = Math.ceil(count / limit) || 0;
    return {
        page: Array.isArray(docs) ? docs : [],
        count: docs.length,
        total_pages,
        total_count: count,
        current_page: (page === undefined ? 1 : page) || null,
        next_page: (page >= total_pages ? null : page + 1),
    }
};