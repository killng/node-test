/**
 * This function will remove all undefined properties
 * @param {Object} data
 * @return {Object}
 * */
module.exports = data =>
    Object.keys(data).forEach(prop => data[prop] === undefined && delete data[prop]);