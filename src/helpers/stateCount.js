const {READY_TO_DISPATCH, FLOWFBS} = require('../heritage/Orders')
const {getDeliveryStates} = require('../heritage/businessStates')
const clearObject = require('./clearObject');
const snakeCase = require('./snakeCase');
/**
 * @param {String} type
 * @param {Number} business
 * @param {Number} companyId
 * @param {String[]} [countryCode]
 * @param {Object} [createdAt]
 * @param {String | Object} [dispatchType]
 * return {Object}
 * */
module.exports = async (type, business, companyId, countryCode, createdAt, dispatchType) => {
    let deliveryStates = getDeliveryStates({business, type, dispatchType});
    if (!(dispatchType && FLOWFBS.indexOf(dispatchType) > -1)) {
        deliveryStates = deliveryStates.filter(i => i !== READY_TO_DISPATCH);
    }
    let results = {};
    let query = {
        dispatchType,
        companyId,
        business,
        countryCode: {$eq: countryCode},
        createdAt,
        type: typeof type === "string" ? type : undefined
    };
    clearObject(query);

    // We don't need count any more!
    for (let deliveryState of deliveryStates) results[snakeCase(deliveryState) + '_count'] = 0;

    results.all_count = Object.values(results).reduce((a, b) => a + b, 0);
    return  results
};