/**
 * This function return value from object with path. If it not exists, it can return a defaultValue or 'undefined'
 * So, you don't need install lodash.
 *
 * @param {Object|Array} data
 * @param {String} path
 * @param {*} [defaultValue]
 * @return {*}
 * */
module.exports = (data, path, defaultValue) => {
    const travel = regexp =>
        String.prototype.split
            .call(path || "", regexp)
            .filter(Boolean)
            .reduce((res, key) => (res !== null && res !== undefined ? res[key] : res), data);
    const result = travel(/[,[\]]+?/) || travel(/[,[\].]+?/);
    return result === undefined || result === data ? defaultValue : result;
};