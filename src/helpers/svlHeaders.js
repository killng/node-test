const toInt = require('./toInt');
const get = require('./get');
const {FULFILLED_BY_SELLER_TYPE} = require('../heritage/Orders')
/**
 * @typedef SvlHeaders
 * @property {Number} business
 * @property {String} userType
 * @property {String} country
 * @property {Number} companyId
 * @property {String} [orderType]
 * */
/**
 * @param {Object} request
 * @return {SvlHeaders}
 * */
module.exports = request => ({
    business: toInt(get(request, 'headers.x-business-id')),
    country: get(request, 'headers.x-svlcountry'),
    companyId: toInt(get(request, 'headers.x-company-id')),
    userType: get(request, 'headers.x-usertype'),
    orderType: get(request, 'headers.x-ordertype', FULFILLED_BY_SELLER_TYPE)
});