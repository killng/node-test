const {morganFormat} = require("../config/express");
const logger = require("../helpers/logger");
const morgan = require("morgan");
const compression = require("compression");
const express = require('express');
const helmet = require('helmet');
const routes = require('../routes');

const http = require('http');
const app = express();

app.disable('x-powered-by');
app.use(helmet());

app.set('port', process.env.EXPRESS_PORT || 3000);
if (process.env.NODE_ENV !== "test") { // disable on unit/integration testing
    if (process.env.EXPRESS_MORGAN === "true") {
        app.use(morgan(morganFormat));
    }
}
if (process.env.EXPRESS_LOG_REQUEST === "true") {
    app.use(({headers, query}, res, next) => {
        logger.info("Express request info", {
            headers,
            query
        });
        next();
    });
}

app.use((err, req, res, next) => { // eslint-disable-line
    logger.error(err.stack);
    res.sendStatus(500).end();
});

app.use(compression());

app.use('/orders', routes);
app.use('/gustavs', routes);

// health check
app.use('/', (req, res) => res.send({ok: true}).end());

http.createServer(app).listen(app.get('port'), () =>
    logger.info( 'Express server listening on port ', app.get('port')));

module.exports = app;