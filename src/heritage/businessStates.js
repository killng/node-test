const get = require('../helpers/get');
const {
    FALABELLA, SODIMAC, FULFILLED_BY_FALABELLA, FULFILLED_BY_SELLER_MARKETPLACE, PURCHASE_ORDER_REPLENISHMENT,
    INTERNATIONAL, EXTERNAL_COURIER, DROPSHIP_ORDER_TYPE,
    EXTERNAL_COURIER_EXPRESS,
    EXTERNAL_COURIER_EXPRESS_DEPRISA,
    CROSS_DOCKING_WITH_3PL,
    SODIMAC_DISPATCH_TYPE
} = require('./Orders');

const getDeliveryStates = ({business, type, dispatchType}) => ({
    [FALABELLA]: falabellaStateMachines(type, dispatchType),
    [SODIMAC]: sodimacStateMachines(type, dispatchType)
}[business] || require('./stateMachines/deliveryStateMachine'));


const EXTERNAL_COURIER_STATE_MACHINE_BY_DISPATCH_TYPE = {
    [EXTERNAL_COURIER_EXPRESS]: require('./stateMachines/falabella/externalCourier/externalCourierStateMachine'),
    [EXTERNAL_COURIER_EXPRESS_DEPRISA]: require('./stateMachines/falabella/externalCourier/externalCourierStateMachine'),
    [CROSS_DOCKING_WITH_3PL]: require('./stateMachines/falabella/cross_docking_with_3pl_state_machine')
};

const falabellaStateMachines = (type, dispatchType) => {
    return ({
        [FULFILLED_BY_FALABELLA]: require('./stateMachines/fbf/deliveryStateMachine'),
        [FULFILLED_BY_SELLER_MARKETPLACE]: require('./stateMachines/deliveryStateMachine'),
        [PURCHASE_ORDER_REPLENISHMENT]: require('./stateMachines/falabella/vendor/purchaseOrderDeliveryStateMachine'),
        [INTERNATIONAL]: get(
            EXTERNAL_COURIER_STATE_MACHINE_BY_DISPATCH_TYPE,
            dispatchType,
            require('./stateMachines/falabella/externalCourier/externalCourierStateMachine')
        ),
        [EXTERNAL_COURIER]: get(EXTERNAL_COURIER_STATE_MACHINE_BY_DISPATCH_TYPE, dispatchType),
        [DROPSHIP_ORDER_TYPE]: require('./stateMachines/falabella/dropship/deliveryStateMachine')
    }[type])
};

const sodimacStateMachines = (type, dispatchType) => ({
    [FULFILLED_BY_SELLER_MARKETPLACE]: get(SODIMAC_STATE_MACHINE_BY_DISPATCH_TYPE, dispatchType),
    [DROPSHIP_ORDER_TYPE]: get(SODIMAC_STATE_MACHINE_BY_DISPATCH_TYPE, dispatchType)
}[type]);


const SODIMAC_STATE_MACHINE_BY_DISPATCH_TYPE = {
    [SODIMAC_DISPATCH_TYPE.CROSS_DOCKING_TO_DELIVERY_CENTER]: require('./stateMachines/sodimac/deliveryStateMachine'),
    [SODIMAC_DISPATCH_TYPE.DIRECT]: require('./stateMachines/sodimac/direct/deliveryStateMachine'),
    [SODIMAC_DISPATCH_TYPE.DIRECT_WITH_COURIER]: require('./stateMachines/sodimac/direct/directWithCourierDeliveryStateMachine')
};

module.exports = {getDeliveryStates};
