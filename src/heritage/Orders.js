const FALABELLA = 0,
    SODIMAC = 1;

const HUMAN_BUSINESS_NAMES = {
    [FALABELLA]: 'Falabella',
    [SODIMAC]: 'Sodimac'
};

const CROSS_DOCKING_EXPRESS = 'cross_docking_express';
const CROSS_DOCKING_WITH_3PL = 'cross_docking_with_3pl';
const FLOWFBS = [CROSS_DOCKING_EXPRESS, CROSS_DOCKING_WITH_3PL];

const EXTERNAL_COURIER_EXPRESS = 'external_courier_express';
const EXTERNAL_COURIER_EXPRESS_DEPRISA = 'external_courier_express_deprisa';
const READY_TO_PICKUP = 'readyToPickUp';
const FULFILLED_BY_SELLER_MARKETPLACE = 'fulfilled_by_seller';
const FULFILLED_BY_FALABELLA = 'fulfilled_by_falabella';
const DROPSHIP_ORDER_TYPE = 'dropship';
const MARKETPLACE_ORDER_TYPE = 'marketplace';
const INTERNATIONAL = 'international';
const PURCHASE_ORDER_REPLENISHMENT = 'purchaseOrderReplenishment';
const FORWAD_PURCHASE_ORDER = 'forwardPurchaseOrder';
const EXTERNAL_COURIER = 'external_courier';

const FULFILLED_BY_SELLER_TYPE = 0,
    FULFILLED_BY_FALABELLA_TYPE = 1,
    PURCHASE_ORDER_TYPE = 2,
    INTERNATIONAL_TYPE = 3,
    EXTERNAL_COURIER_TYPE = 4;

const BUSINESS_ORDER_TYPES = {
    [FALABELLA]: {
        [FULFILLED_BY_SELLER_TYPE]: 'fbs',
        [FULFILLED_BY_FALABELLA_TYPE]: 'fbf',
        [PURCHASE_ORDER_TYPE]: 'purchaseOrder',
        [INTERNATIONAL_TYPE]: 'international',
        [EXTERNAL_COURIER_TYPE]: 'external_courier'
    },
    [SODIMAC]: {
        [FULFILLED_BY_SELLER_TYPE]: 'fbs'
    }
};

const PENDING = 'pending';
const PENDING_WITH_DELIVERY_NOTE_CANCELLED = 'pending_with_delivery_note_cancelled';
const SENT = 'sent';
const OUT_FOR_DELIVERY = 'outForDelivery';
const DELIVERED = 'delivered';
const CANCELLED = 'cancelled';
const CANCELLATION_DATE = 'cancellationDate';
const PROFORMA_URL_PATH = 'proformaStorageUrl';
const PROFORMA_ORDER_TOTAL = 'proformaOrderTotal';
const READY_TO_DISPATCH = 'readyToDispatch';
const READY_TO_SHIP = 'readyToShip';
const RESCHEDULED = 'rescheduled';
const RESCHEDULED_AT = 'rescheduledAt';
const NOT_FOUND = 'Not Found';
const DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ss';
const ASN_DATE_FORMAT = 'DD-MM-YYYYTHH:mm:ss';
const DO_UPDATE = 'DOUpdate';
const AWAITING_CONFIRMATION = 'awaitingConfirmation';
const ASN_IN_TRANSIT = 'InTransit';
const ASN_IN_TRANSIT_INTERNATIONAL = 'InTransitInternational';
const ASN_IN_TRANSIT_CONFIRMED = 'InTransitConfirmed';
const ASN_IN_TRANSIT_RECEIVED_OL = 'InTransitReceivedOL';
const ASN_IN_TRANSIT_OUT_FOR_DELIVERY = 'InTransitOutForDelivery';
const ASN_DELIVERED = 'Delivered';
const ASN_EVENT_NAME = 'asnStatusUpdate';

const SODIMAC_DISPATCH_TYPE = {
    CROSS_DOCKING_TO_DELIVERY_CENTER: 'cross_docking_to_delivery_center',
    DIRECT: 'direct',
    DIRECT_WITH_COURIER: 'direct_with_courier'
};

module.exports = {
    SODIMAC_DISPATCH_TYPE,
    FALABELLA,
    SODIMAC,
    HUMAN_BUSINESS_NAMES,
    FULFILLED_BY_SELLER_TYPE,
    FULFILLED_BY_FALABELLA_TYPE,
    PURCHASE_ORDER_TYPE,
    INTERNATIONAL_TYPE,
    EXTERNAL_COURIER_TYPE,
    BUSINESS_ORDER_TYPES,
    FULFILLED_BY_SELLER_MARKETPLACE,
    FULFILLED_BY_FALABELLA,
    DROPSHIP_ORDER_TYPE,
    MARKETPLACE_ORDER_TYPE,
    INTERNATIONAL,
    FORWAD_PURCHASE_ORDER,
    EXTERNAL_COURIER,
    PENDING,
    PENDING_WITH_DELIVERY_NOTE_CANCELLED,
    SENT,
    OUT_FOR_DELIVERY,
    DELIVERED,
    CANCELLED,
    CANCELLATION_DATE,
    PROFORMA_URL_PATH,
    PROFORMA_ORDER_TOTAL,
    READY_TO_DISPATCH,
    READY_TO_SHIP,
    RESCHEDULED,
    RESCHEDULED_AT,
    NOT_FOUND,
    DATE_FORMAT,
    ASN_DATE_FORMAT,
    DO_UPDATE,
    AWAITING_CONFIRMATION,
    ASN_IN_TRANSIT,
    ASN_IN_TRANSIT_INTERNATIONAL,
    ASN_IN_TRANSIT_CONFIRMED,
    ASN_IN_TRANSIT_RECEIVED_OL,
    ASN_IN_TRANSIT_OUT_FOR_DELIVERY,
    ASN_DELIVERED,
    ASN_EVENT_NAME,
    EXTERNAL_COURIER_EXPRESS,
    EXTERNAL_COURIER_EXPRESS_DEPRISA,
    PURCHASE_ORDER_REPLENISHMENT,
    READY_TO_PICKUP,
    CROSS_DOCKING_EXPRESS,
    CROSS_DOCKING_WITH_3PL,
    FLOWFBS
};
