const {
    RESCHEDULED,
    CANCELLED,
    READY_TO_SHIP,
    DELIVERED,
    SHIPPED,
    PENDING
} = require('../../../Orders');
module.exports = [
    PENDING,
    READY_TO_SHIP,
    SHIPPED,
    DELIVERED,
    CANCELLED,
    RESCHEDULED
];
