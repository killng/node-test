module.exports = [
  'pending',
  'approved',
  'confirmed',
  'partiallyConfirmed',
  'partiallyReceived',
  'received',
  'cancelled',
  'remainingUnitsRejected'
];
