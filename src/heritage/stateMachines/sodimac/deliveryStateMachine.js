module.exports = [
    'awaitingRequest',
    'awaitingConfirmation',
    'readyForPacking',
    'awaitingAsn',
    'readyToDispatch',
    'delivered',
    'closed',
    'canceled'
];
