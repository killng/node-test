const {AWAITING_REQUEST,
  READY_TO_DISPATCH,
  DELIVERED,
  CANCELED} = require('../../../Orders')
module.exports = [
  AWAITING_REQUEST,
  READY_TO_DISPATCH,
  DELIVERED,
  CANCELED
];
