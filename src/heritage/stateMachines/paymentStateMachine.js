const {DRAFT, RELEASED, CANCELLED, SHIPPED} = require('../Orders');
module.exports = [
    DRAFT,
    RELEASED,
    CANCELLED,
    SHIPPED
];
