const {model: Order} = require("../../schemas/Order")
const {model: ExternalCourierOrder} = require("../../schemas/ExternalCourierOrder");
const paginate = require("../../helpers/paginate");
const logger = require("../../helpers/logger")
const svlHeaders = require("../../helpers/svlHeaders")
const get = require("../../helpers/get")
const {mapCountry} = require("../../helpers/countries")
const stateCount = require("../../helpers/stateCount")
const {queryCreateIn, queryCreateEq, queryCreateBetweenDate, safeOrOperator, sortBy} = require("../../helpers/query");
const { Types: { ObjectId } } = require('mongoose');
const { find } = require('lodash');
const {
    CROSS_DOCKING_WITH_LOGISTIC_TRAIN,
    CROSS_DOCKING_EXPRESS,
    CROSS_DOCKING_WITH_3PL,
    EXTERNAL_COURIER_EXPRESS_DEPRISA,
    BUSINESS_ID
} = require('../../config/constants');
const moment = require("moment")

module.exports = async (req, res) => {
  let {business, companyId, country, orderType} = svlHeaders(req);

  // This allows the endpoint to search for any order, regardless of their company id
  // Like an admin endpoint
  const previousCompanyIdValue = get(req, 'headers.x-company-id');
  if (!previousCompanyIdValue || isNaN(previousCompanyIdValue)) companyId = undefined;

  logger.info("Query string", req.query);
  country = mapCountry(get(req.query, 'filter.where.country') || country);
  let pre = {
      or: get(req.query, 'filter.where.or') || undefined,
      deliveryDate: get(req.query, 'filter.where.deliveryDate.between') || undefined,
      deliveryStart: get(req.query, 'filter.where.deliveryStart.between') || undefined,
      createdAt: get(req.query, 'filter.where.createdAt.between') || undefined,
      dispatchType: get(req.query, 'filter.where.dispatchType') || undefined,
      deliveryState: get(req.query, 'filter.where.deliveryState') || undefined,
      type: get(req.query, 'filter.where.type') || undefined,
      sku: get(req.query, 'filter.where.products.elemMatch.sku') || undefined,
      externalId: get(req.query, 'filter.where.externalId') || undefined,
      description: get(req.query, 'filter.where.products.elemMatch.description') || undefined,
      countryCode: country,
      companyId: get(req.query, 'filter.where.companyId') || undefined,
      appointmentId: get(req.query, 'filter.where.appointmentId') || undefined,
      sort: {
          ...sortBy(get(req.query, 'filter.order') || undefined)
      }
	};

  if (
		(
			(
				pre.deliveryState === 'pending' || pre.deliveryState === 'rescheduled'
			)
			||
			find(pre.or, (o) => o.deliveryState === 'pending' || o.deliveryState === 'rescheduled')
		)
		&&
		!pre.deliveryStart
		&&
		(
        pre.dispatchType === CROSS_DOCKING_WITH_LOGISTIC_TRAIN
        ||
        pre.dispatchType === CROSS_DOCKING_EXPRESS
        ||
        pre.dispatchType === CROSS_DOCKING_WITH_3PL
        ||
        pre.dispatchType === EXTERNAL_COURIER_EXPRESS_DEPRISA
    )
  ) {
    pre.deliveryStart = queryCreateBetweenDate(moment().subtract(15, 'days').format('YYYY-MM-DD'), null, false);
	} else if (Array.isArray(pre.deliveryStart) && pre.deliveryStart.length === 2) {
		// "deliveryStart" es un string y no un Date en mongo... 🤦🏻
    pre.deliveryStart = pre.deliveryStart.filter(i => /^\d{4}\-\d{2}\-\d{2}$/.test(i));
    pre.deliveryStart = pre.deliveryStart.length > 0 ? {
        $gte: pre.deliveryStart[0],
        $lt: pre.deliveryStart[1]
    } : undefined;
	} else pre.deliveryStart = undefined;

  if (Array.isArray(pre.deliveryDate) && pre.deliveryDate.length === 2) {
      pre.deliveryDate = queryCreateBetweenDate(pre.deliveryDate[0], pre.deliveryDate[1], true);
  }
  if (Array.isArray(pre.createdAt) && pre.createdAt.length === 2) {
      pre.createdAt = queryCreateBetweenDate(pre.createdAt[0], pre.createdAt[1], true);
  }

  pre.or = safeOrOperator(pre.or);

  const query = {
      deliveryStart: pre.deliveryStart,
      createdAt: pre.createdAt,
      deliveryDate: pre.deliveryDate,
      dispatchType: queryCreateIn(pre.dispatchType),
      deliveryState: queryCreateIn(pre.deliveryState),
      type: queryCreateIn(pre.type),
      externalId: queryCreateEq(pre.externalId),
      "products.description": queryCreateEq(pre.description),
      countryCode: {$eq: pre.countryCode},
      business,
  };

  if (pre.sku) {
    query.$and = [
      {
        $or: [
          {
            "products.sku": pre.sku
          },
          {
            "products.customSKU": pre.sku
          }
        ]
      }
    ]

    if (pre.or) query.$and.push({$or: pre.or});
  } else {
    query.$or = pre.or;
  };

  const finalCompanyId = parseInt(companyId ? companyId : pre.companyId);

  if (finalCompanyId) query.companyId = finalCompanyId;
  if (pre.appointmentId) query.appointmentId = new ObjectId(pre.appointmentId);

  let paginated = await paginate(
      Order,
      {},
      get(req.query, 'limit', 50),
      get(req.query, 'page', 0),
      pre.sort
  );
  let paginatedExternalOrdeCourier = {
      page: []
  };
  if (business == BUSINESS_ID.falabella && pre.dispatchType === CROSS_DOCKING_WITH_3PL && pre.type === 'external_courier') {
    const distributionOrderId = queryCreateIn(paginated.page.map((ele)=> ele.internalId));
    if(distributionOrderId) {
        paginatedExternalOrdeCourier = await paginate(
            ExternalCourierOrder,
            {
                distributionOrderId
            },
            0,
            get(req.query, 'page', 0),
            undefined,
            undefined,
            undefined,
            {distributionOrderId: 1, trackingNumber: 1}
        );
    }
  }

  // generate sellerSku
  paginated.page = paginated.page.map(i => ({
      ...i,
      _id: undefined,
      id: i._id,
      products: i.products.map(({quantity, priceCents, sku, description, lineItem, customSKU}) =>
          ({quantity, priceCents, sku, description, lineItem, sellerSku: customSKU})),
	    trackingNumber: (paginatedExternalOrdeCourier ?
	      get(find(paginatedExternalOrdeCourier.page, (o) => o.distributionOrderId == i.internalId),'trackingNumber', '') : '' )
  }));

  const states = await stateCount(pre.type || orderType, business, companyId, pre.countryCode, pre.createdAt, pre.dispatchType);
  res.send({...states, ...paginated}).end();
};