const moment = require('moment');

module.exports = async (req, res) => {
  return res.send({ hello: moment().format()  }).end();
}