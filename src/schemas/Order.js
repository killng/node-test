const mongoose = require("mongoose")
const schema = new mongoose.Schema({});

let model = mongoose.models.order;
if (typeof model === "undefined") {
    model = mongoose.model('order', schema, 'order')
}

module.exports = {model, schema}