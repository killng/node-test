const mongoose = require("mongoose")
const schema = new mongoose.Schema({});

let model = mongoose.models.DispatchOrder;
if (typeof model === "undefined") {
    model = mongoose.model('DispatchOrder', schema, 'dispatch-order')
}

module.exports = {model, schema}