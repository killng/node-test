const mongoose = require("mongoose")

const schema = new mongoose.Schema({});


let model = mongoose.models.ExternalCourierOrder;
if (typeof model === "undefined") {
    model = mongoose.model('ExternalCourierOrder', schema, 'external-courier-order')
}

module.exports = {model, schema}