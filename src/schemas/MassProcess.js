const mongoose = require("mongoose");
const schema = new mongoose.Schema({});

let model = mongoose.models.MassProcess;
if (typeof model === "undefined") {
    model = mongoose.model('MassProcess', schema, 'mass-process');
}

module.exports = {model, schema};
