const mongoose = require("mongoose")
const schema = new mongoose.Schema({});

let model = mongoose.models.DistributionOrder;
if (typeof model === "undefined") {
    model = mongoose.model('DistributionOrder', schema, 'distribution-order')
}

module.exports = {model, schema}