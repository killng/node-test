const mongoose = require("mongoose")
const schema = new mongoose.Schema({});

let model = mongoose.models.RejectedDistributionOrder;
if (typeof model === "undefined") {
    model = mongoose.model('RejectedDistributionOrder', schema, 'rejected-distribution-order')
}

module.exports = {model, schema}